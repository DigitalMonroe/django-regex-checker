from django.test import TestCase
from django.test import Client


class TestViews(TestCase):
    def setUp(self):
        self.c = Client()

    def test_valid_answer(self):
        request = self.c.post(
            '/', {'get_regex': '[0-9]', 'get_text': '123'}
            )
        self.assertEqual(
            request.context['answer'], 'Регулярное выражение верно!'
            )

    def test_invalid_answer(self):
        request = self.c.post(
            '/', {'get_regex': '[a-b]', 'get_text': '123'}
            )
        self.assertEqual(
            request.context['answer'], 'Регулярное выражение ошибочно!'
            )
