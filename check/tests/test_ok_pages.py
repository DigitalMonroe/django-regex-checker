from django.test import TestCase
from django.test import Client
from http import HTTPStatus


class TestConnect(TestCase):
    def setUp(self):
        self.c = Client()

    def test_is_ok_homepage(self):
        response = self.c.get('/')
        self.assertEqual(response.status_code, HTTPStatus.OK)
